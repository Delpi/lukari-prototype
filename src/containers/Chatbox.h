#pragma once
#include <string>
#include <vector>
#include "raylib.h"

class Chatbox {
 public:
  // General settings (all necessary to run BEFORE using the class)
  static void initStaticFields(int windowWidth);
  static void setColorTheme(Color sentColor, Color receivedColor, Color textColor);
  static void setFontTheme(std::string fontFile, float fSz, float fSep);

  // Constructor wrapper
  static void createMessage(std::string message, bool isOwn);

  // Drawing
  static void drawAllMessages();

  // For later tracking
  static int getLastMessageEnd();

 private:
  // Drawing dimensions and spatial reference points
  inline static int messageMargins;
  inline static int messageSeparation;
  inline static int boxWidth;
  inline static int leftAnchorPoint;
  inline static int rightAnchorPoint;
  inline static int lastMessageEndsAt;

  // Theming aspects
  inline static Color sentMessageBackground;
  inline static Color receivedMessageBackground;
  inline static Color messageTextColor;
  inline static Font messageFont;
  inline static float fontSize;
  inline static float fontSeparation;

  // Instance Fields
  std::string chatMessage;
  Rectangle boxBackground;
  Rectangle messageBound;
  bool wasMessageOwn;

  // Tracking
  inline static std::vector<Chatbox> allMessages;

  // Drawing
  void drawMessage();

  // Constructor
  Chatbox(std::string msg, bool isMessageOwn);
};
