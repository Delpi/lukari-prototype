#include "SceneData.h"

Texture2D SceneData::getBackgroundImage(){
  return backgroundImage;
}

void SceneData::setBackgroundImageFile(std::string s){
  // Hmm... Really necessary to have a mutator? Consider just constructor
  backgroundImageFile = s;
}

void SceneData::loadBackgroundImage() {
  backgroundImage = LoadTexture(backgroundImageFile.c_str());
}

void SceneData::setupCamera(int originalX, int minY, int maxY, int step) {
  sceneCamera.zoom = 1.0f;

  sceneCamera.target.x = originalX;
  sceneCamera.target.y = minY;
  minCameraY = minY;
  maxCameraY = maxY;
  cameraStep = step;
}

void SceneData::setCameraMaxRange(int maxY) {
  maxCameraY = maxY;
}

void SceneData::addRangeToCamera(int extraY) {
  maxCameraY += extraY;
}

void SceneData::focusCameraToBottom() {
  sceneCamera.target.y = maxCameraY;
}

void SceneData::moveCameraUp() {
  if (sceneCamera.target.y - cameraStep > minCameraY) {
    sceneCamera.target.y -= cameraStep;
  } else {
    sceneCamera.target.y = minCameraY;
  }
}

void SceneData::moveCameraDown() {
  if (sceneCamera.target.y + cameraStep < maxCameraY) {
    sceneCamera.target.y += cameraStep;
  } else {
    this->focusCameraToBottom();
  }
}

Camera2D SceneData::getCamera() {
  return sceneCamera;
}
