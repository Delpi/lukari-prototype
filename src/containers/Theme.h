#pragma once

#include "raylib.h"

class Theme {
 public:
  void apply();
  void initTheme(Color background,
                 Color sentMsgs,
                 Color receivedMsgs,
                 Color textMsgs);

 private:
  Color mainBackground;
  Color sentMessageBackground;
  Color receivedMessageBackground;
  Color messageColor;
};
