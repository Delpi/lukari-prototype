#include "Theme.h"
#include "Chatbox.h"
#include "WindowData.h"

void Theme::apply() {
  Chatbox::setColorTheme(sentMessageBackground,
                         receivedMessageBackground,
                         messageColor);
  WindowData::setBackgroundColor(mainBackground);
}

void Theme::initTheme(Color background,
                      Color sentMsgs,
                      Color receivedMsgs,
                      Color textMsgs) {
  mainBackground = background;
  sentMessageBackground = sentMsgs;
  receivedMessageBackground = receivedMsgs;
  messageColor = textMsgs;
}
