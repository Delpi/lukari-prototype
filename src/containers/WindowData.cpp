#include "SceneData.h"
#include <string>
#include "WindowData.h"


WindowData::WindowData (SceneData newMainScene) {
  mainScene = newMainScene;
}

int WindowData::getWindowWidth() {
  return windowWidth;
}
int WindowData::getWindowHeight() {
  return windowHeight;
}
int WindowData::getTargetFPS() {
  return targetFPS;
}

std::string WindowData::getWindowTitle() {
  return windowTitle;
}

SceneData& WindowData::getCurrentScene() {
  switch (currentScene) {
    case SceneType::MainScene: {
      return mainScene;
    }
  }
}

void WindowData::setBackgroundColor(Color color) {
  backgroundColor = color;
}

Color WindowData::getBackgroundColor() {
  return backgroundColor;
}


SceneData& WindowData::getMainScene() {
  return mainScene;
}

Theme& WindowData::getNeutralTheme() {
  return neutralTheme;
}

Theme& WindowData::getAngryTheme() {
  return angryTheme;
}

Theme& WindowData::getSadTheme() {
  return sadTheme;
}

Theme& WindowData::getCurrentMood() {
  switch (currentMood) {
    case Mood::Neutral: {
      return neutralTheme;
    }
    case Mood::Angry: {
      return angryTheme;
    }
    case Mood::Sad: {
      return sadTheme;
    }
  }
}

void WindowData::setCurrentMood(Mood mood) {
  currentMood = mood;
}
