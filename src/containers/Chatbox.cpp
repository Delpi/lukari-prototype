#include "Chatbox.h"

void Chatbox::initStaticFields(int windowWidth) {
  const float ratioBoxToWindow = 0.65f;
  const int edgePadding = 12;
  messageMargins = 5;
  messageSeparation = 10;

  boxWidth = (int) (windowWidth * ratioBoxToWindow);

  leftAnchorPoint = edgePadding;
  rightAnchorPoint = windowWidth - boxWidth - 2 * edgePadding - 2 * messageMargins;
  lastMessageEndsAt = 0;
}

void Chatbox::setColorTheme(Color sentColor, Color receivedColor, Color textColor) {
  messageTextColor = textColor;
  sentMessageBackground = sentColor;
  receivedMessageBackground = receivedColor;
}

void Chatbox::setFontTheme(std::string fontFile, float fSz, float fSep) {
  fontSize = fSz;
  fontSeparation = fSep;
  messageFont = LoadFontEx(fontFile.c_str(), (int)fontSize, 0, 255);
}

void Chatbox::drawMessage() {
  DrawRectangleRec(boxBackground,
                   (wasMessageOwn ? sentMessageBackground : receivedMessageBackground));
  DrawTextRec(messageFont, chatMessage.c_str(), messageBound,
              fontSize, fontSeparation, true, messageTextColor);
}

void Chatbox::drawAllMessages() {
  for (auto message : allMessages) {
    message.drawMessage();
  }
}

Chatbox::Chatbox(std::string msg, bool isMessageOwn) {
  chatMessage = msg;
  wasMessageOwn = isMessageOwn;

  Vector2 textSize = MeasureTextEx(messageFont, msg.c_str(), fontSize, fontSeparation);
  const int allMargins = 2 * messageMargins;
  const int textBoundWidth = boxWidth + allMargins;
  const int expectedLines = textSize.x / textBoundWidth + 1;
  const int textBoundHeight = 1.5 * textSize.y * expectedLines;

  const int xOrigin = isMessageOwn ? rightAnchorPoint : leftAnchorPoint;
  const int yOrigin = lastMessageEndsAt + messageSeparation;

  messageBound = {xOrigin + messageMargins, yOrigin + messageMargins, // origin data
                  textBoundWidth, textBoundHeight}; // size data
  boxBackground = {xOrigin, yOrigin,
                   textBoundWidth + allMargins, textBoundHeight + allMargins};

  // Update class data
  lastMessageEndsAt += messageSeparation + allMargins + textBoundHeight;
}

void Chatbox::createMessage(std::string message, bool isOwn) {
  allMessages.push_back(Chatbox(message, isOwn));
}

int Chatbox::getLastMessageEnd() {
  return lastMessageEndsAt + messageSeparation;
}
