#pragma once

#include <functional>
#include <string>
#include "raylib.h"

class SceneData {
 public:
  Texture2D getBackgroundImage();
  void setBackgroundImageFile(std::string s);
  void loadBackgroundImage(); // requires an OpenGL context

  void setupCamera(int originalX, int minY, int maxY, int step);
  void addRangeToCamera(int extraY);
  void setCameraMaxRange(int maxY);
  void focusCameraToBottom();
  void moveCameraDown();
  void moveCameraUp();
  Camera2D getCamera();

  // lambdas
  std::function<void()> initAction;
  std::function<void()> preCameraAction;
  std::function<void()> inCameraAction;
  std::function<void()> postCameraAction;
  std::function<void()> interactiveStepAction;

 private:
  Texture2D backgroundImage;
  std::string backgroundImageFile;

  // Camera handling
  Camera2D sceneCamera;
  int minCameraY;
  int maxCameraY;
  int cameraStep;
};
