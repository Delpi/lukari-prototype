#pragma once
#include "SceneData.h"
#include "Theme.h"
#include <string>
#include "raylib.h"

enum class SceneType {MainScene}; // Expand as needed

enum class Mood {Neutral, Angry, Sad}; // Expand as needed

class WindowData {
  // Note: stick to one instance only
 public:
  WindowData (SceneData newMainScene);

  int getWindowWidth();
  int getWindowHeight();
  int getTargetFPS();
  std::string getWindowTitle();
  SceneData& getCurrentScene();
  Theme& getCurrentMood();
  void setCurrentMood(Mood newMood);

  // Color management
  static void setBackgroundColor(Color color);
  static Color getBackgroundColor();

  // Accessors for all scenes (add as needed, ofc)
  SceneData& getMainScene();

  // Accessors for all moods (add as needed)
  Theme& getNeutralTheme();
  Theme& getAngryTheme();
  Theme& getSadTheme();

 private:
  int windowWidth = 405;
  int windowHeight = 720;
  int targetFPS = 30;
  std::string windowTitle = "foo!";
  inline static Color backgroundColor;

  // The following variables concern themselves with scenes
  SceneType currentScene = SceneType::MainScene;
  SceneData mainScene;

  // The following members store a theme for each mood
  Mood currentMood = Mood::Neutral;
  Theme neutralTheme;
  Theme angryTheme;
  Theme sadTheme;
};
