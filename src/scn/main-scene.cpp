#include <vector>
#include "raylib.h"
#include "main-scene.h"
#include "SceneData.h"
#include "WindowData.h"
#include "Chatbox.h"

void mainSceneConfigure (SceneData &scene, WindowData &window) {

  scene.initAction = [&scene, &window] () { mainSceneInit(scene, window); };
  scene.preCameraAction = [&scene, &window] () { mainScenePreCamera(scene, window); };
  scene.inCameraAction = [&scene, &window] () { mainSceneCameraMode(scene, window); };
  scene.postCameraAction = [&scene, &window] () { mainScenePostCamera(scene, window); };
  scene.interactiveStepAction = [&scene, &window] () { mainSceneInteraction(scene, window); };
}

void mainSceneInit (SceneData& scene, WindowData& window) {
  scene.loadBackgroundImage();

  Chatbox::initStaticFields(window.getWindowWidth());
  Chatbox::setFontTheme("res/NotoSans-Regular.ttf", 20.0f, 0.5f);

  // Test messages! Do spare me from my suffering and develop a way to read from files...
  Chatbox::createMessage("quo usque tandem abutere, Catilina, patientia nostra? quam diu etiam furor iste tuus nos eludet?", true);
  Chatbox::createMessage("quem ad finem sese effrenata iactabit audacia? nihilne te nocturnum praesidium Palati, nihil urbis vigiliae, nihil timor populi, nihil concursus bonorum omnium, nihil hic munitissimus habendi senatus locus, nihil horum ora voltusque moverunt?", false);
  Chatbox::createMessage("patere tua consilia non sentis, constrictam iam horum omnium scientia teneri coniurationem tuam non vides? quid proxima, quid superiore nocte egeris, ubi fueris, quos convocaveris, quid consili ceperis quem nostrum ignorare arbitraris?", false);
  Chatbox::createMessage("O tempora, o mores!", true);
  Chatbox::createMessage("senatus haec intellegit, consul videt; hic tamen vivit.", true);
  Chatbox::createMessage("vivit?", false);
  Chatbox::createMessage("immo vero etiam in senatum venit, fit publici consili particeps, notat et designat oculis ad caedem unum quemque nostrum. nos autem fortes viri satis facere rei publicae videmur, si istius furorem ac tela vitamus.", true);
  Chatbox::createMessage("ad mortem te, Catilina, duci iussu consulis iam pridem oportebat, in te conferri pestem quam tu in nos omnis iam diu machinaris.", false);

  // Camera preparation
  int lastCameraPosition = Chatbox::getLastMessageEnd() - window.getWindowHeight();
  int cameraStep = 10;
  scene.setupCamera(0, 0, lastCameraPosition, cameraStep);
  scene.focusCameraToBottom();
}

void mainScenePreCamera (SceneData& scene, WindowData& window) {
  DrawTexture(scene.getBackgroundImage(), 0, 0, WHITE);
  window.getCurrentMood().apply();
  DrawRectangle(0, 0, window.getWindowWidth(), window.getWindowHeight(), WindowData::getBackgroundColor());
}

void mainSceneCameraMode (SceneData& scene, WindowData& window) {
  BeginMode2D(scene.getCamera());
  Chatbox::drawAllMessages();
  EndMode2D();
}

void mainScenePostCamera (SceneData& scene, WindowData& window) {
  // foo!
}

void mainSceneInteraction (SceneData& scene, WindowData& window) {
  // Camera displacement (arrowkeys)
  if (IsKeyDown(KEY_DOWN)) {
    scene.moveCameraDown();
  } else if (IsKeyDown(KEY_UP)) {
    scene.moveCameraUp();
  }

  // Mood settings (numbers)
  if (IsKeyPressed(KEY_ONE)) {
    window.setCurrentMood(Mood::Neutral);
  } else if (IsKeyPressed(KEY_TWO)) {
    window.setCurrentMood(Mood::Angry);
  } else if (IsKeyPressed(KEY_THREE)) {
    window.setCurrentMood(Mood::Sad);
  }
}
