#pragma once
#include "SceneData.h"
#include "WindowData.h"

void mainSceneConfigure (SceneData& scene, WindowData& window);
void mainSceneInit (SceneData& scene, WindowData& window);
void mainScenePreCamera (SceneData& scene, WindowData& window);
void mainSceneCameraMode (SceneData& scene, WindowData& window);
void mainScenePostCamera (SceneData& scene, WindowData& window);
void mainSceneInteraction (SceneData& scene, WindowData& window);
