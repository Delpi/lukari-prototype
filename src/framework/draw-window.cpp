#include <string>
#include "raylib.h"
#include "SceneData.h"
#include "WindowData.h"
#include "window.h"
#include "main-scene.h"

void loadWindow(WindowData& details) {
  // Starts an OpenGL context
  InitWindow(details.getWindowWidth(),
             details.getWindowHeight(),
             details.getWindowTitle().c_str());
  SetTargetFPS(details.getTargetFPS());
}

void createWindow(WindowData& details) {
  loadWindow(details);
  mainSceneConfigure(details.getMainScene(), details);
  details.getMainScene().initAction();
  while (!WindowShouldClose()) {
    SceneData currentScene = details.getCurrentScene();
    currentScene.interactiveStepAction();

    BeginDrawing();
    ClearBackground(WHITE);

    currentScene.preCameraAction();
    currentScene.inCameraAction();

    EndDrawing();
  }
  CloseWindow();
}
