#include "containers/WindowData.h"
#include "framework/window.h"
#include "containers/SceneData.h"
#include "raylib.h"

int main(){
  SceneData mainScene;
  mainScene.setBackgroundImageFile("res/main-background-bw.png");
  Color backColor = {126, 140, 126, 175};
  WindowData::setBackgroundColor(backColor);
  WindowData details(mainScene);
  details.getNeutralTheme().initTheme({126, 140, 126, 175},
                                    {124, 196, 159, 255},
                                    {88, 166, 126, 255},
                                    BLACK);
  details.getAngryTheme().initTheme({165, 75, 75, 175},
                                    {230, 184, 184, 255},
                                    {166, 66, 68, 255},
                                    BLACK);
  details.getSadTheme().initTheme({71, 129, 143, 175},
                                  {163, 176, 181, 255},
                                  {77, 124, 140, 255},
                                  BLACK);
  createWindow(details);
  return 0;
}
