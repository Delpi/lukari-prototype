# Private notes on the prototype

## Program structure

**main.cpp** handles loading and generating the data needed for the game.

As for the contents of **framework/**,
**draw-window.cpp** has the functions necessary to draw a window through raylib. All the code responsible for loading textures or other resources that require an OpenGL context is called there.

The class **WindowData** is in charge of storing and passing the information needed for each window to be created.
This includes the data needed before starting the OpenGL context (such as the dimensions of the generated window and target FPS), and other objects detailing the state of the canvas at any given point (menu overlays, main menu, game screen). Those last ones correspond to the class **SceneData**.

The class **Chatbox** has the data needed for each message from the chat interface


## Drawing and Scenes

As far as the engine is concerned, a Scene is an isolated object that describes both the data represented in the screen and the user's interaction with it. 

### Methods and times

There are five four different stages at which scene-specific data is interacted with:

1. Upon creation of the OpenGL context. This largely covers the loading of elements into graphic memory.
2. Pre-camera layer. Involves background elements and others such that will neither be displaced relative to a camera, zoomed, or rotated, but should be covered by those that do
3. Camera layer. Elements drawn here are subjected to movement by the target of the scene camera.
4. Post-camera layer. Mostly concerned with UI elements that are also not affected by the camera, but are drawn on top of them.
5. Input layer. Listens for user interaction and gives control to the relevant function or method.

The program must provide the functions for each stage, which will be added as a function object to the SceneData object.
