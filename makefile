APP = lukari-prototype

VPATH = src/:src/containers:src/framework:src/scn
OBJDIR := obj
OBJS = main.o \
       Chatbox.o SceneData.o WindowData.o Theme.o\
       draw-window.o \
       main-scene.o

CC = g++
FLAGS = -Wall -Wformat -Wformat-security -pedantic -Wextra -Wformat-nonliteral -Wformat=2
LIBS = /usr/local/lib/libraylib.so
LINKER_EXTRAS = -lraylib   -lGL  -lm -pthread -ldl -lX11
INCLUDES = -Isrc -Isrc/containers -Isrc/framework -Isrc/scn
BASIC_OBJECT_FORMULA = $(FLAGS) $(INCLUDES) -o

bin/lukari-prototype: $(OBJS)
	$(CC) -o bin/$(APP) obj/*.o $(LIBS) $(LINKER_EXTRAS) $(INCLUDES)

main.o:
	$(CC) -c src/main.cpp $(BASIC_OBJECT_FORMULA) obj/main.o
Chatbox.o:
	$(CC) -c src/containers/Chatbox.cpp $(BASIC_OBJECT_FORMULA) obj/Chatbox.o
SceneData.o:
	$(CC) -c src/containers/SceneData.cpp $(BASIC_OBJECT_FORMULA) obj/SceneData.o
WindowData.o:
	$(CC) -c src/containers/WindowData.cpp $(BASIC_OBJECT_FORMULA) obj/WindowData.o
Theme.o:
	$(CC) -c src/containers/Theme.cpp $(BASIC_OBJECT_FORMULA) obj/Theme.o
draw-window.o:
	$(CC) -c src/framework/draw-window.cpp $(BASIC_OBJECT_FORMULA) obj/draw-window.o
main-scene.o:
	$(CC) -c src/scn/main-scene.cpp $(BASIC_OBJECT_FORMULA) obj/main-scene.o

.PHONY = clean
.PHONY = all
.PHONY = debug
clean:
	rm -f obj/*
all: $(OBJS)
debug: bin/lukari-prototype
	./bin/$(APP)
